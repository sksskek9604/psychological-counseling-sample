package com.amh.psychologicalcounselingsample.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CenterRequest {

    @NotNull
    @ApiModelProperty(notes = "지역", required = true)
    private String area;

    @NotNull
    @ApiModelProperty(notes = "센터명", required = true)
    private String centerName;

    @NotNull
    @ApiModelProperty(notes = "센터주소", required = true)
    private String centerAddress;

    @NotNull
    @ApiModelProperty(notes = "센터번호", required = true)
    private String centerTelephoneNumber;

    @NotNull
    @ApiModelProperty(notes = "진료시간", required = true)
    private String workingTime;
}
