package com.amh.psychologicalcounselingsample.service;

import com.amh.psychologicalcounselingsample.entity.Center;
import com.amh.psychologicalcounselingsample.model.CenterRequest;
import com.amh.psychologicalcounselingsample.repository.CenterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CenterService {

    private final CenterRepository centerRepository;

    public void setCenter(CenterRequest request) {
        Center center = new Center.CenterBuilder(request).build();
        centerRepository.save(center);
    }
}
