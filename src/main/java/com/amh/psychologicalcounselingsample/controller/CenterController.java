package com.amh.psychologicalcounselingsample.controller;

import com.amh.psychologicalcounselingsample.model.CenterRequest;
import com.amh.psychologicalcounselingsample.model.CommonResult;
import com.amh.psychologicalcounselingsample.service.CenterService;
import com.amh.psychologicalcounselingsample.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "센터 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/center")
public class CenterController {

    private final CenterService centerService;

    @ApiOperation(value = "센터 등록")
    @PostMapping("/new")
    public CommonResult setCenter(@RequestBody @Valid CenterRequest request) {
        centerService.setCenter(request);
        return ResponseService.getSuccessResult();
    }
}
