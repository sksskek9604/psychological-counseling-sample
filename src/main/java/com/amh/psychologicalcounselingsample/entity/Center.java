package com.amh.psychologicalcounselingsample.entity;

import com.amh.psychologicalcounselingsample.interfaces.CommonModelBuilder;
import com.amh.psychologicalcounselingsample.model.CenterRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Center {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String area;

    @Column(nullable = false, length = 20)
    private String centerName;

    @Column(nullable = false, length = 40)
    private String centerAddress;

    @Column(nullable = false, length = 20)
    private String centerTelephoneNumber;

    @Column(nullable = false, length = 20)
    private String workingTime;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;


    private Center(CenterBuilder builder) {
        this.area = builder.area;
        this.centerName = builder.centerName;
        this.centerAddress = builder.centerAddress;
        this.centerTelephoneNumber = builder.centerTelephoneNumber;
        this.workingTime = builder.workingTime;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class CenterBuilder implements CommonModelBuilder<Center> {

        private final String area;
        private final String centerName;
        private final String centerAddress;
        private final String centerTelephoneNumber;
        private final String workingTime;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public CenterBuilder(CenterRequest request) {

            this.area = request.getArea();
            this.centerName = request.getCenterName();
            this.centerAddress = request.getCenterAddress();
            this.centerTelephoneNumber = request.getCenterTelephoneNumber();
            this.workingTime = request.getWorkingTime();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();

        }

        @Override
        public Center build() {
            return new Center(this);
        }
    }
}
