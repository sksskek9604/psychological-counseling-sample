package com.amh.psychologicalcounselingsample.repository;

import com.amh.psychologicalcounselingsample.entity.Center;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CenterRepository extends JpaRepository<Center, Long> {
}
