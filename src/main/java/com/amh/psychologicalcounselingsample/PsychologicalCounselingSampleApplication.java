package com.amh.psychologicalcounselingsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PsychologicalCounselingSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(PsychologicalCounselingSampleApplication.class, args);
    }

}
