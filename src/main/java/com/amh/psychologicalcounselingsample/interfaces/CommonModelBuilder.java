package com.amh.psychologicalcounselingsample.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
